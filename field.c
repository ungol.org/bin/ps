#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ps.h"

struct field * ps_addfield(struct field *head, char *name)
{
	struct def {
		enum ps_field type;
		char *name;
		char *header;
		int size;
	} defaults[] = {
		{ PS_ARGS,	"args",		"COMMAND",	0 },
		{ PS_COMM,	"comm",		"COMMAND",	0 },
		{ PS_ETIME,	"etime",	"ELAPSED",	0 },
		{ PS_GROUP,	"group",	"GROUP",	8 },
		{ PS_NICE,	"nice",		"NICE",		0 },
		{ PS_PCPU,	"pcpu",		"%CPU",		0 },
		{ PS_PGID,	"pgid",		"PGID",		5 },
		{ PS_PID,	"pid",		"PID",		5 },
		{ PS_PPID,	"ppid",		"PPID",		5 },
		{ PS_RGROUP,	"rgroup",	"RGROUP",	8 },
		{ PS_RUSER,	"ruser",	"RUSER",	8 },
		{ PS_TIME,	"time",		"TIME",		0 },
		{ PS_TTY,	"tty",		"TT",		0 },
		{ PS_USER,	"user",		"USER",		8 },
		{ PS_VSZ,	"vsz",		"VSZ",		8 },
		/* uppercase names are internally used by -l and -f, but
		 * are not available to -o */
		{ PS_F,		"F",		"F",		0 },
		{ PS_S,		"S",		"S",		0 },
		{ PS_UID,	"UID",		"UID",		8 },
		{ PS_STIME,	"STIME",	"STIME",	0 },
		{ PS_PRI,	"PRI",		"PRI",		0 },
		{ PS_ADDR,	"ADDR",		"ADDR",		0 },
		{ PS_WCHAN,	"WCHAN",	"WCHAN",	0 },
		{ 0,		NULL,		NULL,		0 },
	};

	char *equals = strchr(name, '=');

	struct def *h = NULL;
	for (h = defaults; h->name != NULL; h++) {
		if (strcmp(name, h->name) == 0) {
			break;
		}
		if (strncmp(name, h->name, strlen(h->name)) == 0 && name[strlen(h->name)] == '=') {
			break;
		}
	}

	if (h->name == NULL) {
		fprintf(stderr, "invalid name '%s'\n", name);
		return NULL;
	}

	struct field *f = malloc(sizeof(*f));
	if (f == NULL) {
		perror("ps: malloc");
		return NULL;
	}

	f->next = NULL;
	f->type = h->type;
	f->header = equals ? strdup(equals + 1) : h->header;
	f->size = strlen(h->header);
	if (f->header != h->header && f->header[0] != '\0') {
		f->size = strlen(f->header);
	}
	if (f->size < h->size) {
		f->size = h->size;
	}

	if (!head) {
		return f;
	}

	struct field *p = head;
	while (p->next) {
		p = p->next;
	}
	p->next = f;

	return head;
}

void ps_freefield(struct field *field)
{
	if (field == NULL) {
		return;
	}

	while (field) {
		struct field *next = field->next;
		free(field);
		field = next;
	}
}
