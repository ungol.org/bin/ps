#define _XOPEN_SOURCE 700
#include <grp.h>
#include <pwd.h>
#include <string.h>
#include <stdio.h>

#include "ps.h"

static char *ps_username(intmax_t uid, int maxwidth)
{
	static char name[64];
	struct passwd *user = getpwuid((uid_t)uid);
	if (user) {
		strcpy(name, user->pw_name);
	} else {
		snprintf(name, sizeof(name), "%jd", uid);
	}
	if (maxwidth > 0) {
		if (strlen(name) > (size_t)maxwidth) {
			name[maxwidth - 1] = '+';
			name[maxwidth] = '\0';
		}
	}
	return name;
}

static char *ps_groupname(intmax_t gid, int maxwidth)
{
	static char name[64];
	struct group *group = getgrgid((gid_t)gid);
	if (group) {
		strcpy(name, group->gr_name);
	} else {
		snprintf(name, sizeof(name), "%jd", gid);
	}
	if (maxwidth > 0) {
		if (strlen(name) > (size_t)maxwidth) {
			name[maxwidth - 1] = '+';
			name[maxwidth] = '\0';
		}
	}
	return name;
}

void ps_headers(struct field *fields)
{
	int display = 0;
	
	for (struct field *p = fields; p != NULL; p = p->next) {
		if (fields->header[0] != '\0') {
			display = 1;
			break;
		}
	}

	if (!display) {
		return;
	}

	for (struct field *p = fields; p != NULL; p = p->next) {
		printf("%*s", p->size, p->header);
		if (p->next) {
			printf(" ");
		}
	}

	printf("\n");
}

static void ps_printfield(struct process *p, int width, enum ps_field type, int trunc)
{
	switch (type) {
	case PS_ADDR:
		printf("%*s", width, "-");
		break;

	case PS_ARGS:
		printf("%-*s", width, p->args);
		break;

	case PS_COMM:
		printf("%-*s", width, p->comm);
		break;

	case PS_ETIME:
		printf("%*jd", width, p->time);	/* FIXME */
		break;

	case PS_F:
		printf("%*ju", width, p->flags);
		break;

	case PS_GROUP:
		printf("%-*s", width, ps_groupname(p->egid, trunc ? width : 0));
		break;

	case PS_NICE:
		printf("%*jd", width, p->nice);
		break;

	case PS_PCPU:
		printf("%*jd", width, p->cpu);
		break;

	case PS_PGID:
		printf("%*jd", width, p->pgid);
		break;

	case PS_PID:
		printf("%*jd", width, p->pid);
		break;

	case PS_PPID:
		printf("%*jd", width, p->ppid);
		break;

	case PS_PRI:
		printf("%*jd", width, p->priority);
		break;

	case PS_RGROUP:
		printf("%-*s", width, ps_groupname(p->rgid, trunc ? width : 0));
		break;

	case PS_RUSER:
		printf("%-*s", width, ps_username(p->ruid, trunc ? width : 0));
		break;

	case PS_STIME:
		printf("%*ju", width, p->time); /* FIXME: start time */
		break;

	case PS_S:
		printf("%*c", width, p->state);
		break;

	case PS_TIME:
		printf("%*ju", width, p->time);
		break;

	case PS_TTY:
		printf("%-*s", width, p->tty);
		break;

	case PS_UID:
		printf("%*jd", width, p->euid);
		break;

	case PS_USER:
		printf("%-*s", width, ps_username(p->euid, trunc ? width : 0));
		break;

	case PS_VSZ:
		printf("%*ju", width, p->size);
		break;

	case PS_WCHAN:
		printf("%*ju", width, p->wchan);
		break;
	}
}

void ps_print(struct process *ps, struct field *fields)
{
	for (struct field *p = fields; p != NULL; p = p->next) {
		ps_printfield(ps, p->size, p->type, p->next != NULL);
		if (p->next) {
			printf(" ");
		}
	}
	printf("\n");
}
