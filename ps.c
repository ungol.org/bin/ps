#define _XOPEN_SOURCE 700
#include <ctype.h>
#include <errno.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "ps.h"

int main(int argc, char *argv[])
{
	struct field *fields = NULL;
	int all = 0;
	int all_terms = 0;
	int almost_all = 0;
	struct list *sessions = NULL;
	struct list *rgids = NULL;
	struct list *pids = NULL;
	struct list *terms = NULL;
	struct list *euids = NULL;
	struct list *ruids = NULL;
	struct list *out = NULL;

	int c;
	while ((c = getopt(argc, argv, "aAdeflg:G:n:o:p:t:u:U:")) != -1) {
		switch (c) {
		case 'a':	/* all processes associated with a terminal */
				/* may omit session leaders */
			all_terms = 1;
			break;

		case 'A':	/* all processes */
		case 'e':	/* all processes */
			all = 1;
			break;

		case 'd':	/* all processes except session leaders */
			almost_all = 1;
			break;

		case 'f':	/* generate a full listing */
			ps_freefield(fields);
			fields = ps_addfield(NULL, "user=UID");
			fields = ps_addfield(fields, "pid");
			fields = ps_addfield(fields, "ppid");
			fields = ps_addfield(fields, "pcpu=C");
			fields = ps_addfield(fields, "STIME");
			fields = ps_addfield(fields, "tty=TTY");
			fields = ps_addfield(fields, "time");
			fields = ps_addfield(fields, "args=CMD");
			/* uid is user name */
			break;

		case 'g':	/* all processes with session leaders */
			sessions = ps_makelist(optarg);
			break;

		case 'G':	/* all processes with real group id */
			rgids = ps_makelist(optarg);
			break;

		case 'l':	/* generate a long listing */
			ps_freefield(fields);
			fields = ps_addfield(NULL, "F");
			fields = ps_addfield(fields, "S");
			fields = ps_addfield(fields, "UID");
			fields = ps_addfield(fields, "pid");
			fields = ps_addfield(fields, "ppid");
			fields = ps_addfield(fields, "pcpu=C");
			fields = ps_addfield(fields, "PRI");
			fields = ps_addfield(fields, "nice=NI");
			fields = ps_addfield(fields, "ADDR");
			fields = ps_addfield(fields, "vsz=SZ");
			fields = ps_addfield(fields, "WCHAN");
			fields = ps_addfield(fields, "tty=TTY");
			fields = ps_addfield(fields, "time");
			fields = ps_addfield(fields, "comm=CMD");
			break;

		case 'n':	/* specify an alternative namelist file */
			/* ignored for compatiblity with XSI, which does not
			 * actually specify what a namelist means */
			break;

		/* FIXME: everything after the first equals sign is the
		 * header name for that option */
		case 'o':	/* specify the output format */
			out = ps_makelist(optarg);
			for (struct list *p = out; p != NULL; p = p->next) {
				if (islower(p->value[0])) {
					fields = ps_addfield(fields, p->value);
				} else {
					fprintf(stderr, "ps: invalid name '%s'\n", optarg);
					return 1;
				}
			}
			ps_freelist(out);
			break;

		case 'p':	/* processes in list */
			pids = ps_makelist(optarg);
			break;

		case 't':	/* processes associated with terminals */
			terms = ps_makelist(optarg);
			break;

		case 'u':	/* processes matching user ids */
			euids = ps_makelist(optarg);
			break;

		case 'U':	/* processes matching real user ids */
			ruids = ps_makelist(optarg);
			break;

		default:
			return 1;
		}
	}

	if (fields == NULL) {
		fields = ps_addfield(fields, "pid");
		fields = ps_addfield(fields, "tty=TTY");
		fields = ps_addfield(fields, "time");
		fields = ps_addfield(fields, "comm=CMD");
	}

	if (optind != argc) {
		fprintf(stderr, "ps: unexpected operands\n");
		return 1;
	}

	const char *procpath = "/sys/linux/proc";
	DIR *procdir = opendir(procpath);
	if (procdir == NULL) {
		if (errno == ENOENT) {
			procpath = "/proc";
			procdir = opendir(procpath);
		}
	}

	if (procdir == NULL) {
		fprintf(stderr, "ps: %s: %s\n", procpath, strerror(errno));
		return 1;
	}

	ps_headers(fields);
	struct dirent *de = NULL;
	intmax_t euid = geteuid();
	while ((de = readdir(procdir)) != NULL) {
		struct process *ps = ps_read(dirfd(procdir), de->d_name);
		char buf[128];

		if (!ps) {
			continue;
		}
		if (all) {
			goto print;
		}
		if (almost_all /* && !is_session_leader(ps)) */) {
			goto print;
		}
		if (all_terms && ps->tty[0] != '\0' /* && !sessionleader */) {
			goto print;
		}

		if (ps->euid == euid /* && same controlling terminal */) {
			goto print;
		}

		if (sessions) {
			//snprintf(buf, sizeof(buf), "%jd", ps->session));
			if (ps_in_list(buf, sessions)) {
				goto print;
			}
		}
		if (rgids) {
			snprintf(buf, sizeof(buf), "%jd", ps->rgid);
			if (ps_in_list(buf, rgids)) {
				goto print;
			}
		}
		if (pids) {
			snprintf(buf, sizeof(buf), "%jd", ps->pid);
			if (ps_in_list(buf, pids)) {
				goto print;
			}
		}
		if (terms) {
			if (ps_in_list(ps->tty, terms)) {
				goto print;
			}
		}
		if (euids) {
			snprintf(buf, sizeof(buf), "%jd", ps->euid);
			if (ps_in_list(buf, euids)) {
				goto print;
			}
		}
		if (ruids) {
			snprintf(buf, sizeof(buf), "%jd", ps->ruid);
			if (ps_in_list(buf, ruids)) {
				goto print;
			}
		}
	
		continue;

		print:
			ps_print(ps, fields);
	}

	closedir(procdir);
	return 0;
}
