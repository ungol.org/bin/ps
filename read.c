#define _XOPEN_SOURCE 700
#include <ctype.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/* http://manpages.courier-mta.org/htmlman5/proc.5.html */

#include "ps.h"

static ssize_t ps_readfile(int dirfd, char *path, size_t n, char buf[
	#ifndef __TINYC__
	static n
	#endif
])
{
	int fd = openat(dirfd, path, O_RDONLY);
	if (fd == -1) {
		return -1;
	}

	ssize_t nread = read(fd, buf, n);

	close(fd);

	return nread;
}

static inline int ps_startswith(const char *s, const char *prefix)
{
	return strncmp(s, prefix, strlen(prefix)) == 0;
}

static inline char *ps_value(char *s)
{
	char *start = strchr(s, ':');
	if (!start) {
		return NULL;
	}
	start++;
	while (isspace(*start)) {
		start++;
	}
	return start;
}

struct process *ps_read(int procfd, char *pids)
{
	static struct process p;
	//intmax_t tty_nr;
	//uintmax_t utime, stime;

	struct process *ret = NULL;
	int piddir = -1, statfd = -1;
	FILE *f = NULL;
	char *buf = NULL;
	size_t bufsize = 0;

	if (!isdigit(*pids)) {
		goto end;
	}

	piddir = openat(procfd, pids, O_DIRECTORY | O_SEARCH);
	if (piddir == -1) {
		goto end;
	}

	memset(&p, '\0', sizeof(p));

	ps_readfile(piddir, "cmdline", sizeof(p.args), p.args);
	for (size_t i = 0; i < sizeof(p.args) - 1; i++) {
		if (p.args[i] == '\0') {
			if (p.args[i + 1] == '\0') {
				break;
			}
			p.args[i] = ' ';
		}
	}

	statfd = openat(piddir, "status", O_RDONLY);
	if (statfd == -1) {
		goto end;
	}

	f = fdopen(statfd, "r");
	if (f == NULL) {
		goto end;
	}
	
	while (getline(&buf, &bufsize, f) > 0) {
		if (ps_startswith(buf, "Name:")) {
			strcpy(p.comm, ps_value(buf));
			char *newline = strchr(p.comm, '\n');
			if (newline) {
				*newline = '\0';
			}
		} else if (ps_startswith(buf, "State:")) {
			p.state = *ps_value(buf);
		} else if (ps_startswith(buf, "Pid:")) {
			p.pid = strtoimax(ps_value(buf), NULL, 10);
		} else if (ps_startswith(buf, "PPid:")) {
			p.ppid = strtoimax(ps_value(buf), NULL, 10);
		} else if (ps_startswith(buf, "Uid:")) {
			sscanf(buf, "Uid: %jd %jd %*d %*d", &p.ruid, &p.euid);
		} else if (ps_startswith(buf, "Gid:")) {
			sscanf(buf, "Gid: %jd %jd %*d %*d", &p.rgid, &p.egid);
		} else if (ps_startswith(buf, "VmSize:")) {
			p.size = strtoumax(ps_value(buf), NULL, 10);
		}
	}

	ret = &p;

end:
	if (buf != NULL) {
		free(buf);
	}

	if (f != NULL) {
		fclose(f);
	}

	if (statfd != -1) {
		close(statfd);
	}

	if (piddir != -1) {
		close(piddir);
	}

	return ret;
}
