#define _XOPEN_SOURCE 700
#include <string.h>
#include <stdlib.h>
#include "ps.h"

struct list *ps_makelist(char *arg)
{
	struct list *head = NULL;
	struct list *tail = head;

	while (*arg) {
		struct list *tmp = calloc(1, sizeof(*tail));
		if (head == NULL) {
			head = tmp;
			tail = tmp;
		} else {
			tail->next = tmp;
			tail = tail->next;
		}

		char *nextarg = arg;
		while ((*nextarg != ',') && (*nextarg != ' ') && (*nextarg != '\0')) {
			nextarg++;
		}
		tail->value = strndup(arg, nextarg - arg);
		arg = nextarg;
		while (*arg == ',' || *arg == ' ') {
			/* skip extra commas or spaces */
			arg++;
		}
	}
	
	return head;
}

int ps_in_list(const char *value, struct list *list)
{
	while (list != NULL) {
		if (!strcmp(value, list->value)) {
			return 1;
		}
		list = list->next;
	}
	return 0;
}

void ps_freelist(struct list *list)
{
	if (list == NULL) {
		return;
	}

	while (list != NULL) {
		struct list *next = list->next;
		free(list);
		list = next;
	}
}
