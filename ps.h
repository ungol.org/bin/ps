#define _XOPEN_SOURCE 700
#include <fcntl.h>
#include <inttypes.h>
#include <limits.h>

#ifndef PATH_MAX
#define PATH_MAX _POSIX_PATH_MAX
#endif

#ifndef O_SEARCH
#define O_SEARCH O_RDONLY
#endif

struct process {
	intmax_t pid;
	intmax_t pgid;
	intmax_t ppid;
	intmax_t ruid;
	intmax_t euid;
	intmax_t rgid;
	intmax_t egid;
	intmax_t session;
	uintmax_t flags;
	char state;
	uintmax_t cpu;
	intmax_t priority;
	intmax_t nice;
	uintptr_t address;
	uintmax_t size;
	uintmax_t wchan;
	uintmax_t start;
	uintmax_t time;
	char tty[PATH_MAX];
	char comm[PATH_MAX];
	char args[PATH_MAX];
};

struct list {
	struct list *next;
	char *value;
};

enum ps_field {
	PS_ADDR,
	PS_ARGS,
	PS_COMM,
	PS_ETIME,
	PS_F,
	PS_GROUP,
	PS_NICE,
	PS_PCPU,
	PS_PGID,
	PS_PID,
	PS_PPID,
	PS_PRI,
	PS_RGROUP,
	PS_RUSER,
	PS_STIME,
	PS_S,
	PS_TIME,
	PS_TTY,
	PS_UID,
	PS_USER,
	PS_VSZ,
	PS_WCHAN,
};

struct field {
	struct field *next;
	enum ps_field type;
	char *header;
	int size;
};

struct process *ps_read(int procfd, char *pids);

struct field * ps_addfield(struct field *head, char *name);
void ps_freefield(struct field *head);

void ps_headers(struct field *fields);
void ps_print(struct process *p, struct field *fields);

int ps_session(struct process *ps, char *list);
int ps_rgid(struct process *ps, char *list);
int ps_pid(struct process *ps, char *list);
int ps_term(struct process *ps, char *list);
int ps_euid(struct process *ps, char *list);
int ps_ruid(struct process *ps, char *list);

struct list *ps_makelist(char *arg);
int ps_in_list(const char *value, struct list *list);
void ps_freelist(struct list *list);
